﻿class MainClass
{
    public static void Main(string[] args)
    {
        Cat cat = new Cat("", "");
        Animal animal = cat;
        animal.GetSound();
        Console.WriteLine("------");
        GetAnimalSound(cat);
    }

    public static void GetAnimalSound(Animal animal)
    {
        animal.GetSound();
    }

}

abstract class Animal
{
    private string Name;
    private string Country;

    public Animal(string name, string country)
    {
        Name = name;
        Country = country;
    }

    public abstract void GetSound();
}

class Cat : Animal
{
    public Cat(string name, string country) : base(name, country)
    {

    }

    public override void GetSound()
    {
        Console.WriteLine("Mew, mew");
    ;}
}

class Dog: Animal
{
    public Dog(string name, string country): base(name, country)
    {

    }

    public override void GetSound()
    {
        Console.WriteLine("Gav gav ");
    }
}






























/*

class MainClass
{
    public static void Main(string[] args)
    {
        Person person = new Person("Misha", "Ivanov", 22);
        Person person1 = new Person("Dmitry", "Ivanov", 42);


        if (person == person1)
        {
            Console.WriteLine("The same");
        }
        else
        {
            Console.WriteLine("Not the same");
        }

        ////////////

        float float_val = 5.9f;
        double double_val = 6.0;
        double_val = float_val;
        float_val = (float)double_val;

        int int_val;

        int_val = (int)float_val;

        Console.WriteLine(int_val);
    }

}

class Person : Object
{
    private string name;
    private string surname;
    private int age;

    public string Name => name;
    public string Surname => surname;
    public int Age => age;


    public Person(string name, string surname, int age)
    {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }


    public override string ToString()
    {
        return $"Name: {name}\nSurname: {surname}\nAge:{age}";
    }

    public override bool Equals(object obj)
    {
        Person person = (Person)obj;

        if (Name == person.Name && Surname == person.Surname && Age == person.Age)
        {
            return true;
        }
        else
        {
            return false;
        }
    }




static bool operator ==(Person person, Person person1)           // c# < 8.0, поэому убрали оверрайд
    {
        if (person.Name == person1.Name && person.Surname == person1.Surname && person.Age == person1.Age)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static bool operator !=(Person person, Person person1)
    {
        if (person.Name != person1.Name || person.Surname != person1.Surname || person.Age != person1.Age)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
*/